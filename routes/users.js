const express = require('express');
const router = express.Router();

const UserController = require('../controllers/Users/UserController')
const uploadMulter = require('../helpers/upload-multer')

router.get('/', UserController.index);
router.post('/', uploadMulter.any(), UserController.store);
router.put('/:id', UserController.update);
router.delete('/:id', UserController.destroy);
router.get('/search', UserController.search);

module.exports = router;
