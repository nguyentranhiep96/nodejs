const multerU = require('multer');
const path = require('path')
const storage = multerU.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/');
    },
    filename: (req, file, cb) => {
        cb(null , `${Date.now()}`+file.originalname);
    }
})

const upload = multerU({storage:storage});

module.exports = upload;