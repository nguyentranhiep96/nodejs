const userService  = require('../../services/Users/UserService')
module.exports = {
    async index(req, res) {
        await userService.getUser(parseInt(req.query.limit), parseInt(req.query.skip)).then(users => {
            res.status(200).send({
                message: 'Success',
                users: users
            });
        }).catch(error => res.status(400).send('Create Error' +error))
    },
    async store(req, res) {
        const files = req.files;
        let dataFile = [];
        files.forEach((value, key) => {
            let id = key + 1;
            let name = value.filename;
            let dataItem = {
                id: id,
                filename: name,
                chooseAvatar: req.body.chooseavatar
            }
            dataFile.push(dataItem)
        })
        await userService.createUser({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            image: JSON.stringify(dataFile),
            avatar: req.body.chooseavatar
        }).then(user => {
            res.status(201).send({
                message: 'Create Success',
                user: user
            });
        }).catch(error => res.status(400).send('Create Error' +error))
    },
    async update(req, res) {
        await userService.updateUser({
            first_name: req.body.first_name
        }, req.params.id).then(() => {
            res.status(200).send({
                message: 'Update Success',
            });
        }).catch(error => res.status(400).send('Update Error' +error))
    },
    async destroy(req, res) {
        const ids = req.params.id.split(',')
        await userService.deleteUser(ids).then(() => {
            res.status(200).send({
                message: 'Delete Success',
            });
        }).catch(error => res.status(400).send('Delete Error' +error))
    },
    async search(req, res) {
        const query = req.query.first_name
        await userService.searchUser(query).then((users) => {
            res.status(200).send({
                message: 'Search Success',
                users: users
            });
        }).catch(error => res.status(400).send('Search Error' +error))
    }
}