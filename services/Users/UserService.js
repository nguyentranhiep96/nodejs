const { Op } = require("sequelize");
const model = require('../../models');

module.exports = {
    async getUser(limit = 10, offset = 0) {
        return await model.User.findAll({
            limit: limit,
            offset: offset
        })
    },
    async createUser(data = []) {
        return await model.User.create(data)
    },
    async updateUser(data = [], id) {
        return await model.User.update(data, { where: { id: id } })
    },
    async deleteUser(ids = []) {
        return await model.User.destroy({ where: { id: ids } })
    },
    async searchUser(query) {
        return await model.User.findAll({ where : {
                first_name: {
                    [Op.like]: query,
                }
            }
        })
    }
}